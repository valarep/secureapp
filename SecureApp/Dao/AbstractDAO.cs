﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace SecureApp.Dao
{
    abstract class AbstractDao
    {
        protected MySqlConnection Connection;
        private const string server = "127.0.0.1";
        private const string port = "3306";
        private const string uid = "secure_app";
        private const string password = "9IQ4hjREBB0rEPEQ";
        private const string database = "secure_app";
        private const string charset = "utf8";
        public string ConnectionString
        {
            get
            {
                return
                    "Server=" + server + ";" +
                    "Port=" + port + ";" +
                    "Uid=" + uid + ";" +
                    "Password=" + password + ";" +
                    "Database=" + database + ";" +
                    "Charset=" + charset + ";";
            }
        }

        public AbstractDao()
        {
            Connection = new MySqlConnection();
            Connection.ConnectionString = ConnectionString;
        }
    }
}
