﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SecureApp.Classes;
using SecureApp.Dao;

namespace SecureApp
{
    public partial class FrmUser : Form
    {
        enum Column { Pseudo, Email, Edit, Delete }
        public List<Utilisateur> Items { get; set; }
        public Utilisateur SelectedItem { get; set; }

        public FrmUser()
        {
            InitializeComponent();
        }

        private void FrmUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            FrmMain frmMain = (FrmMain)MdiParent;
            frmMain.frmUser = null;
        }

        private void FrmUser_Load(object sender, EventArgs e)
        {
            UtilisateurDao dao = new UtilisateurDao();
            Items = dao.GetUtilisateurs();
            dataGridView1.Columns.Add("pseudo", "Pseudo");
            dataGridView1.Columns.Add("email", "Email");

            DataGridViewButtonColumn btnEdit = new DataGridViewButtonColumn();
            dataGridView1.Columns.Add(btnEdit);
            btnEdit.HeaderText = "";
            btnEdit.Text = "Edit";
            btnEdit.Name = "btnEdit";
            btnEdit.Width = 50;
            btnEdit.UseColumnTextForButtonValue = true;

            DataGridViewButtonColumn btnDelete = new DataGridViewButtonColumn();
            dataGridView1.Columns.Add(btnDelete);
            btnDelete.HeaderText = "";
            btnDelete.Text = "Delete";
            btnDelete.Name = "btnDelete";
            btnDelete.Width = 50;
            btnDelete.UseColumnTextForButtonValue = true;

            foreach (Utilisateur utilisateur in Items)
            {
                dataGridView1.Rows.Add(utilisateur.Pseudo, utilisateur.Email);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Column column = (Column)e.ColumnIndex;
            if (column == Column.Edit)
            {
                SelectedItem = Items[e.RowIndex];
                FrmUserEdit frm = new FrmUserEdit();
                frm.Tag = this;
                DialogResult result = frm.ShowDialog();
            }
        }
    }
}
