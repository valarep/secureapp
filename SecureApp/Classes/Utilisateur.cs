﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureApp.Dao;

namespace SecureApp.Classes
{
    public class Utilisateur
    {
        public long? Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public string Pseudo { get; set; }
        public string MotDePasse { get; set; }
        public long? IdService { get; set; }
        private Service service = null;
        public Service Service
        {
            get
            {
                if (service == null && IdService != null)
                {
                    ServiceDao dao = new ServiceDao();
                    service = dao.GetService(IdService);
                }
                return service;
            }

            set
            {
                service = value;
                IdService = service.Id;
            }
        }

        private List<Groupe> groupes = null;
        public List<Groupe> Groupes
        {
            get
            {
                if (groupes == null && Id != null)
                {
                    GroupeDao dao = new GroupeDao();
                    groupes = dao.GetGroupesFromUtilisateur((long) Id);
                }
                return groupes;
            }
        }

        private List<Droit> droits = null;
        public List<Droit> Droits
        {
            get
            {
                if (droits == null && Id != null)
                {
                    DroitDao dao = new DroitDao();
                    droits = dao.GetDroitsFromUtilisateur((long) Id);
                }
                return droits;
            }
        }

        public Utilisateur() { }

        public Utilisateur(long id, string pseudo, string motdepasse, long? idService)
        {
            Id = id;
            Pseudo = pseudo;
            MotDePasse = motdepasse;
            IdService = idService;
        }
    }
}
