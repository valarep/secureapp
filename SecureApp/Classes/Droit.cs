﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureApp.Classes
{
    public class Droit
    {
        public long? Id { get; set; }
        public string Nom { get; set; }

        public Droit() { }

        public Droit(long id, string nom)
        {
            Id = id;
            Nom = nom;
        }

        public override string ToString()
        {
            return Nom;
        }
    }
}
