﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureApp.Classes
{
    public class Groupe : IComparable<Groupe>
    {
        public long? Id { get; set; }
        public string Nom { get; set; }

        List<Droit> Droits { get; set; }

        public Groupe() { }

        public Groupe(long id, string nom)
        {
            Id = id;
            Nom = nom;
        }

        public override string ToString()
        {
            return Nom;
        }

        public int CompareTo(Groupe other)
        {
            return Nom.CompareTo(other.Nom);
        }
    }
}
