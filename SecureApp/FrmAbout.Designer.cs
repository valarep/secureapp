﻿namespace SecureApp
{
    partial class FrmAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblApplicationName = new System.Windows.Forms.Label();
            this.lblApplicationVersion = new System.Windows.Forms.Label();
            this.lblApplicationCopyright = new System.Windows.Forms.Label();
            this.lblAllRightsReserved = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblApplicationName
            // 
            this.lblApplicationName.AutoSize = true;
            this.lblApplicationName.Location = new System.Drawing.Point(12, 130);
            this.lblApplicationName.Name = "lblApplicationName";
            this.lblApplicationName.Size = new System.Drawing.Size(90, 13);
            this.lblApplicationName.TabIndex = 0;
            this.lblApplicationName.Text = "Secure App 2018";
            // 
            // lblApplicationVersion
            // 
            this.lblApplicationVersion.AutoSize = true;
            this.lblApplicationVersion.Location = new System.Drawing.Point(12, 148);
            this.lblApplicationVersion.Name = "lblApplicationVersion";
            this.lblApplicationVersion.Size = new System.Drawing.Size(75, 13);
            this.lblApplicationVersion.TabIndex = 1;
            this.lblApplicationVersion.Text = "Version 18.1.1";
            // 
            // lblApplicationCopyright
            // 
            this.lblApplicationCopyright.AutoSize = true;
            this.lblApplicationCopyright.Location = new System.Drawing.Point(12, 166);
            this.lblApplicationCopyright.Name = "lblApplicationCopyright";
            this.lblApplicationCopyright.Size = new System.Drawing.Size(139, 13);
            this.lblApplicationCopyright.TabIndex = 2;
            this.lblApplicationCopyright.Text = "© 2018 Lycée Henri Wallon";
            // 
            // lblAllRightsReserved
            // 
            this.lblAllRightsReserved.AutoSize = true;
            this.lblAllRightsReserved.Location = new System.Drawing.Point(12, 184);
            this.lblAllRightsReserved.Name = "lblAllRightsReserved";
            this.lblAllRightsReserved.Size = new System.Drawing.Size(90, 13);
            this.lblAllRightsReserved.TabIndex = 3;
            this.lblAllRightsReserved.Text = "All rights reserved";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(197, 226);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FrmAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblAllRightsReserved);
            this.Controls.Add(this.lblApplicationCopyright);
            this.Controls.Add(this.lblApplicationVersion);
            this.Controls.Add(this.lblApplicationName);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAbout";
            this.Text = "À propos de Secure App";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblApplicationName;
        private System.Windows.Forms.Label lblApplicationVersion;
        private System.Windows.Forms.Label lblApplicationCopyright;
        private System.Windows.Forms.Label lblAllRightsReserved;
        private System.Windows.Forms.Button btnOK;
    }
}