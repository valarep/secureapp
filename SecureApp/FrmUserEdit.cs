﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SecureApp.Classes;
using SecureApp.Dao;

namespace SecureApp
{
    public partial class FrmUserEdit : Form
    {
        public Utilisateur Utilisateur { get; set; }
        List<Groupe> notAffectedGroupes;
        List<Groupe> affectedGroupes;
        List<Service> services;

        public FrmUserEdit()
        {
            InitializeComponent();
        }

        private void FrmUserEdit_Load(object sender, EventArgs e)
        {
            FrmUser frmUser = (FrmUser)Tag;
            Utilisateur = frmUser.SelectedItem;
            txtPseudo.Text = Utilisateur.Pseudo;
            txtNom.Text = Utilisateur.Nom;
            txtPrenom.Text = Utilisateur.Prenom;
            txtEmail.Text = Utilisateur.Email;
            affectedGroupes = Utilisateur.Groupes;
            lstAffected.DataSource = affectedGroupes;
            GroupeDao groupeDao = new GroupeDao();
            notAffectedGroupes = groupeDao.GetNotAffectedGroupesFromUtilisateur((long)Utilisateur.Id);
            lstNotAffected.DataSource = notAffectedGroupes;
            ServiceDao serviceDao = new ServiceDao();
            services = serviceDao.GetServices();
            services.Insert(0, new Service() { Nom = "Aucun" });
            cmbService.DataSource = services;
            Service service = Utilisateur.Service;
            if(service == null)
            {
                cmbService.SelectedIndex = 0;
            }
            else
            {
                service = services.Find(s => s.Id == Utilisateur.Service.Id);
                cmbService.SelectedItem = service;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // update user info
            // update linked groups
            DialogResult = DialogResult.OK;
        }

        private void btnAffect_Click(object sender, EventArgs e)
        {
            if (lstNotAffected.SelectedItems.Count > 0)
            {
                foreach (object item in lstNotAffected.SelectedItems)
                {
                    Groupe groupe = (Groupe)item;
                    notAffectedGroupes.Remove(groupe);
                    affectedGroupes.Add(groupe);
                    affectedGroupes.Sort();
                }
                lstNotAffected.DataSource = null;
                lstNotAffected.DataSource = notAffectedGroupes;
                lstAffected.DataSource = null;
                lstAffected.DataSource = affectedGroupes;

                if (notAffectedGroupes.Count == 0)
                {
                    btnAffect.Enabled = false;
                }
                if (affectedGroupes.Count > 0)
                {
                    btnDisaffect.Enabled = true;
                }
            }
        }

        private void btnDisaffect_Click(object sender, EventArgs e)
        {
            if (lstAffected.SelectedItems.Count > 0)
            {
                foreach (object item in lstAffected.SelectedItems)
                {
                    Groupe groupe = (Groupe)item;
                    affectedGroupes.Remove(groupe);
                    notAffectedGroupes.Add(groupe);
                    notAffectedGroupes.Sort();
                }
                lstNotAffected.DataSource = null;
                lstNotAffected.DataSource = notAffectedGroupes;
                lstAffected.DataSource = null;
                lstAffected.DataSource = affectedGroupes;

                if (notAffectedGroupes.Count > 0)
                {
                    btnAffect.Enabled = true;
                }
                if (affectedGroupes.Count == 0)
                {
                    btnDisaffect.Enabled = false;
                }
            }
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            FrmUserChangePassword frmUserChangePassword = new FrmUserChangePassword();
            DialogResult result = frmUserChangePassword.ShowDialog();
        }
    }
}
