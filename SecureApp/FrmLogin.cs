﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SecureApp.Dao;
using SecureApp.Classes;
using System.Security.Cryptography;

namespace SecureApp
{
    public partial class FrmLogin : Form
    {
        public Utilisateur Utilisateur { get; set; }

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnConnexion_Click(object sender, EventArgs e)
        {
            string pseudo = txtPseudo.Text;
            string motdepasse;
            using (MD5 md5hash = MD5.Create())
            {
                motdepasse = GetMd5Hash(md5hash, txtMotDePasse.Text);
            }

            UtilisateurDao dao = new UtilisateurDao();
            Utilisateur = dao.GetUtilisateur(pseudo, motdepasse);
            if (Utilisateur != null)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Erreur de pseudo ou de mot de passe.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

    }
}
