﻿namespace SecureApp
{
    partial class FrmUserEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPseudo = new System.Windows.Forms.TextBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblPseudo = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblPrenom = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.lblMotDePasse = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpGroupes = new System.Windows.Forms.GroupBox();
            this.btnDisaffect = new System.Windows.Forms.Button();
            this.btnAffect = new System.Windows.Forms.Button();
            this.lstAffected = new System.Windows.Forms.ListBox();
            this.lstNotAffected = new System.Windows.Forms.ListBox();
            this.lblAffected = new System.Windows.Forms.Label();
            this.lblNotAffected = new System.Windows.Forms.Label();
            this.cmbService = new System.Windows.Forms.ComboBox();
            this.lblService = new System.Windows.Forms.Label();
            this.grpGroupes.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPseudo
            // 
            this.txtPseudo.Location = new System.Drawing.Point(127, 15);
            this.txtPseudo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPseudo.Name = "txtPseudo";
            this.txtPseudo.Size = new System.Drawing.Size(132, 22);
            this.txtPseudo.TabIndex = 0;
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(127, 82);
            this.txtNom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(132, 22);
            this.txtNom.TabIndex = 1;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Location = new System.Drawing.Point(127, 114);
            this.txtPrenom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(132, 22);
            this.txtPrenom.TabIndex = 2;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(127, 146);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(132, 22);
            this.txtEmail.TabIndex = 3;
            // 
            // lblPseudo
            // 
            this.lblPseudo.AutoSize = true;
            this.lblPseudo.Location = new System.Drawing.Point(13, 18);
            this.lblPseudo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPseudo.Name = "lblPseudo";
            this.lblPseudo.Size = new System.Drawing.Size(64, 17);
            this.lblPseudo.TabIndex = 4;
            this.lblPseudo.Text = "Pseudo :";
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(13, 85);
            this.lblNom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(45, 17);
            this.lblNom.TabIndex = 5;
            this.lblNom.Text = "Nom :";
            // 
            // lblPrenom
            // 
            this.lblPrenom.AutoSize = true;
            this.lblPrenom.Location = new System.Drawing.Point(13, 117);
            this.lblPrenom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrenom.Name = "lblPrenom";
            this.lblPrenom.Size = new System.Drawing.Size(65, 17);
            this.lblPrenom.TabIndex = 6;
            this.lblPrenom.Text = "Prénom :";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(13, 149);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(50, 17);
            this.lblEmail.TabIndex = 7;
            this.lblEmail.Text = "Email :";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(127, 47);
            this.btnChangePassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(100, 28);
            this.btnChangePassword.TabIndex = 8;
            this.btnChangePassword.Text = "Modifier ...";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // lblMotDePasse
            // 
            this.lblMotDePasse.AutoSize = true;
            this.lblMotDePasse.Location = new System.Drawing.Point(13, 53);
            this.lblMotDePasse.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMotDePasse.Name = "lblMotDePasse";
            this.lblMotDePasse.Size = new System.Drawing.Size(101, 17);
            this.lblMotDePasse.TabIndex = 9;
            this.lblMotDePasse.Text = "Mot de passe :";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(448, 191);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(561, 191);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grpGroupes
            // 
            this.grpGroupes.Controls.Add(this.btnDisaffect);
            this.grpGroupes.Controls.Add(this.btnAffect);
            this.grpGroupes.Controls.Add(this.lstAffected);
            this.grpGroupes.Controls.Add(this.lstNotAffected);
            this.grpGroupes.Controls.Add(this.lblAffected);
            this.grpGroupes.Controls.Add(this.lblNotAffected);
            this.grpGroupes.Location = new System.Drawing.Point(268, 15);
            this.grpGroupes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpGroupes.Name = "grpGroupes";
            this.grpGroupes.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpGroupes.Size = new System.Drawing.Size(393, 169);
            this.grpGroupes.TabIndex = 12;
            this.grpGroupes.TabStop = false;
            this.grpGroupes.Text = "Groupes";
            // 
            // btnDisaffect
            // 
            this.btnDisaffect.Location = new System.Drawing.Point(180, 101);
            this.btnDisaffect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDisaffect.Name = "btnDisaffect";
            this.btnDisaffect.Size = new System.Drawing.Size(31, 28);
            this.btnDisaffect.TabIndex = 5;
            this.btnDisaffect.Text = "<";
            this.btnDisaffect.UseVisualStyleBackColor = true;
            this.btnDisaffect.Click += new System.EventHandler(this.btnDisaffect_Click);
            // 
            // btnAffect
            // 
            this.btnAffect.Location = new System.Drawing.Point(180, 65);
            this.btnAffect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAffect.Name = "btnAffect";
            this.btnAffect.Size = new System.Drawing.Size(31, 28);
            this.btnAffect.TabIndex = 4;
            this.btnAffect.Text = ">";
            this.btnAffect.UseVisualStyleBackColor = true;
            this.btnAffect.Click += new System.EventHandler(this.btnAffect_Click);
            // 
            // lstAffected
            // 
            this.lstAffected.FormattingEnabled = true;
            this.lstAffected.ItemHeight = 16;
            this.lstAffected.Location = new System.Drawing.Point(219, 38);
            this.lstAffected.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstAffected.Name = "lstAffected";
            this.lstAffected.Size = new System.Drawing.Size(159, 116);
            this.lstAffected.TabIndex = 3;
            // 
            // lstNotAffected
            // 
            this.lstNotAffected.FormattingEnabled = true;
            this.lstNotAffected.ItemHeight = 16;
            this.lstNotAffected.Location = new System.Drawing.Point(12, 39);
            this.lstNotAffected.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstNotAffected.Name = "lstNotAffected";
            this.lstNotAffected.Size = new System.Drawing.Size(159, 116);
            this.lstNotAffected.TabIndex = 2;
            // 
            // lblAffected
            // 
            this.lblAffected.AutoSize = true;
            this.lblAffected.Location = new System.Drawing.Point(215, 20);
            this.lblAffected.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAffected.Name = "lblAffected";
            this.lblAffected.Size = new System.Drawing.Size(69, 17);
            this.lblAffected.TabIndex = 1;
            this.lblAffected.Text = "Affecté(s)";
            // 
            // lblNotAffected
            // 
            this.lblNotAffected.AutoSize = true;
            this.lblNotAffected.Location = new System.Drawing.Point(8, 20);
            this.lblNotAffected.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNotAffected.Name = "lblNotAffected";
            this.lblNotAffected.Size = new System.Drawing.Size(100, 17);
            this.lblNotAffected.TabIndex = 0;
            this.lblNotAffected.Text = "Non-Affecté(s)";
            // 
            // cmbService
            // 
            this.cmbService.FormattingEnabled = true;
            this.cmbService.Location = new System.Drawing.Point(127, 175);
            this.cmbService.Name = "cmbService";
            this.cmbService.Size = new System.Drawing.Size(121, 24);
            this.cmbService.TabIndex = 13;
            // 
            // lblService
            // 
            this.lblService.AutoSize = true;
            this.lblService.Location = new System.Drawing.Point(13, 178);
            this.lblService.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblService.Name = "lblService";
            this.lblService.Size = new System.Drawing.Size(63, 17);
            this.lblService.TabIndex = 14;
            this.lblService.Text = "Service :";
            // 
            // FrmUserEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 228);
            this.Controls.Add(this.lblService);
            this.Controls.Add(this.cmbService);
            this.Controls.Add(this.grpGroupes);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblMotDePasse);
            this.Controls.Add(this.btnChangePassword);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblPrenom);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.lblPseudo);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtPrenom);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.txtPseudo);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUserEdit";
            this.Text = "Utilisateur";
            this.Load += new System.EventHandler(this.FrmUserEdit_Load);
            this.grpGroupes.ResumeLayout(false);
            this.grpGroupes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPseudo;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblPseudo;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblPrenom;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.Label lblMotDePasse;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox grpGroupes;
        private System.Windows.Forms.Button btnAffect;
        private System.Windows.Forms.ListBox lstAffected;
        private System.Windows.Forms.ListBox lstNotAffected;
        private System.Windows.Forms.Label lblAffected;
        private System.Windows.Forms.Label lblNotAffected;
        private System.Windows.Forms.Button btnDisaffect;
        private System.Windows.Forms.ComboBox cmbService;
        private System.Windows.Forms.Label lblService;
    }
}