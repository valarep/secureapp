﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureApp
{
    public partial class FrmUserChangePassword : Form
    {
        public FrmUserChangePassword()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool error = true;
            if (txtPassword.Text != txtCheckPassword.Text)
            {
                MessageBox.Show("Les mots de passes saisis sont différents.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (txtPassword.Text.Length < 8)
                {
                    MessageBox.Show("Le mot de passe doit au moins 8 caractères.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    error = false;
                    DialogResult = DialogResult.OK;
                }
            }
            if (error)
            {
                txtPassword.Text = "";
                txtCheckPassword.Text = "";
                txtPassword.Focus();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
